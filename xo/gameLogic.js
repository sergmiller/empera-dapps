//XO
function GetLib()
{
    return require(8);//List-lib
}


function getFieldSize() {
    return 3;
}

function getRowsToWin() {
    return 3;
}

function getTurnFee() {
    return 1e-9;
}

function FieldToString(field, winSide, isLastTurn) {
    var N = getFieldSize()
    var res = ""
    for (var i = 0;i < N; ++i) {
        for( var j = 0;j < N; ++j) {
            var c = field[i][j]
            if (c == null) {
                c = '-'
            }
            if (c == 1) {
                c = 'O'
            }
            if (c == 0) {
                c = 'X'
            }
            res += c
        }
        res += "\n"
    }
    if (!(winSide === null)) {
        var c = "Winner is "
        if (winSide == 0) {
            c += 'X'
        } else {
            c += 'O'
        }
        res += c
    }
    if (isLastTurn) {
        res += "There is a draw"
    }
    return res
}

function buildEmptyField() {
    var N = getFieldSize();
    var field = []
    for (var i = 0;i < N; ++i) {
        var row = []
        for (var j = 0; j < N; ++j) {
            row.push(null);
        }
        field.push(row);
    }
    return field;
}

function IsWin(field, i, j, di, dj, side) {
    var N = getFieldSize();
    var T = getRowsToWin();
    for (var t = 0; t < T;++t) {
        var x = i + di * t;
        var y = j + dj * t;
        if (!(0 <= x && x < N && 0 <= y && y < N)) {
            return false;
        }
        if (!(field[x][y] == side)) {
            return false;
        }
    }
    return true;
}

function CalcWinSideIfExists(field) {
    var N = getFieldSize();
    for (var side = 0; side < 2; ++side) {
        for (var i = 0; i < N; ++i) {
            for (var j = 0;j < N;++j) {
                for(var di = -1; di <= 1; ++di) {
                    for (var dj = -1; dj <= 1; ++dj) {
                        if (di == 0 && dj == 0) {
                            continue;
                        }
                        if (IsWin(field, i, j, di, dj, side)) {
                            return side;
                        }
                    }
                }
            }
        }
    }
    return null;
}

"public"
function RestartGame() {
    var contractState = ReadState(context.Smart.Account);
    contractState.field = buildEmptyField();
    contractState.nextMove = 0;
    WriteState(contractState);
}

"public"
function MakeTurn(params) {
    var turnFee = getTurnFee()
    Move(context.FromNum,context.Smart.Account,turnFee,"Turn fee");

    var moveX = parseInt(params.moveX);
    var moveY = parseInt(params.moveY);
    var N = getFieldSize();
    var contractState = ReadState(context.Smart.Account);
    if (contractState.nextMove === undefined) {
        Event("RestartGame first time");
        RestartGame();
    }
    if (!(0 <= moveX && moveX < N && 0 <= moveY && moveY < N)) {
        throw "illegal coordinates";
    }
    Event("move is " + moveX + ", " + moveY);
    if (!(contractState.field[moveX][moveY] === null)) {
        throw "field is not empty";
    }
    var side = contractState.nextMove % 2;
    Event("make turn with side " + side + " at " + moveX + ", " + moveY);
    contractState.field[moveX][moveY] = side;

    var winSide = CalcWinSideIfExists(contractState.field);
    var isLastTurn = false;
    if (contractState.nextMove == N * N - 1) {
        isLastTurn = true;
    }
    Event("Board:" + FieldToString(contractState.field, winSide, isLastTurn))

    if (isLastTurn) {
        Event("There is a draw");
        RestartGame();
        return;
    }
    if (winSide == 0) {
        Event("Winner is first player");
        RestartGame();
        return;
    }
    if (winSide == 1) {
        Event("Winner is second player");
        RestartGame();
        return;
    }
    contractState.nextMove += 1;
    WriteState(contractState);
}

function OnGet()
{
    var lib=GetLib();
    if(lib.OnGet())
        return;

    if(context.Account.Num===context.Smart.Account)
    {
        if(context.Description==="Turn fee")
            return;
        else
            throw "Error description";
    }
}
